
#ifndef __UAO__UaoClientForOpcUaSca__GpioBitBanger__
#define __UAO__UaoClientForOpcUaSca__GpioBitBanger__

#include <iostream>
#include <uaclientcpp/uaclientsdk.h>

namespace UaoClientForOpcUaSca
{

using namespace UaClientSdk;



class GpioBitBanger
{

public:

    GpioBitBanger(
        UaSession* session,
        UaNodeId objId
    );

// getters, setters for all variables

    void bitBang(
        const UaByteString&  in_requestsMessageSerialized,
        std::vector<OpcUa_UInt32>& out_readRegisterData
    );


private:

    UaSession  * m_session;
    UaNodeId     m_objId;

};



}

#endif // __UAO__UaoClientForOpcUaSca__GpioBitBanger__

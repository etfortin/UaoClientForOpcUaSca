
#ifndef __UAO__UaoClientForOpcUaSca__JtagPort__
#define __UAO__UaoClientForOpcUaSca__JtagPort__

#include <iostream>
#include <uaclientcpp/uaclientsdk.h>

namespace UaoClientForOpcUaSca
{

using namespace UaClientSdk;



class JtagPort
{

public:

    JtagPort(
        UaSession* session,
        UaNodeId objId
    );

// getters, setters for all variables
    UaString readState (
        UaStatus     *out_status=nullptr,
        UaDateTime   *sourceTimeStamp=nullptr,
        UaDateTime   *serverTimeStamp=nullptr);

    void writeState (
        UaString& data,
        UaStatus *out_status=nullptr);

    void shiftIr(
        OpcUa_UInt32 in_numberOfBits,
        const UaByteString&  in_tdoBits,
        OpcUa_Boolean in_readTdi,
        UaByteString& out_tdiBits
    );

    void shiftDr(
        OpcUa_UInt32 in_numberOfBits,
        const UaByteString&  in_tdoBits,
        OpcUa_Boolean in_readTdi,
        UaByteString& out_tdiBits
    );

    void shift(
        OpcUa_UInt32 in_numberOfBits,
        const UaByteString&  in_tmsBits,
        const UaByteString&  in_tdoBits,
        UaByteString& out_tdiBits
    );


private:

    UaSession  * m_session;
    UaNodeId     m_objId;

};



}

#endif // __UAO__UaoClientForOpcUaSca__JtagPort__

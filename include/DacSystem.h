
#ifndef __UAO__UaoClientForOpcUaSca__DacSystem__
#define __UAO__UaoClientForOpcUaSca__DacSystem__

#include <iostream>
#include <uaclientcpp/uaclientsdk.h>

namespace UaoClientForOpcUaSca
{

using namespace UaClientSdk;



class DacSystem
{

public:

    DacSystem(
        UaSession* session,
        UaNodeId objId
    );

// getters, setters for all variables


private:

    UaSession  * m_session;
    UaNodeId     m_objId;

};



}

#endif // __UAO__UaoClientForOpcUaSca__DacSystem__
